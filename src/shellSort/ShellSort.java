package shellSort;

import common.SortTestHelper;

/**
 * Create by Persar on 2018/5/7
 * 希尔排序：原理和插入排序相似，也是在寻找元素arr[i]应该插入的位置，不过它是跳跃性的插入
 * 随着跳跃的距离之间减小，数组也慢慢变为有序
 * 其复杂度分析比较难，具体的根据跳跃的间隔而定，一种情况下是O(n^3/2)
 */
public class ShellSort {
    private ShellSort(){}

    public static void sort(Comparable[] arr){
        int n = arr.length;
        //跳跃的序列
        // 计算 increment sequence: 1, 4, 13, 40, 121, 364, 1093...
        int h = 1;
        //计算开始进行排序的最大跳跃值
        while (h < n/3)
            h = 3*h + 1;

        //当跳跃的间隔值满足>=1时，可以执行排序操作，否则排序已经完成
        while (h >= 1) {
            // h-sort the array
            for (int i = h; i < n; i++) {

                // 对 arr[i], arr[i-h], arr[i-2*h], arr[i-3*h]... 使用插入排序
                Comparable e = arr[i];
                int j = i;
                for ( ; j >= h && e.compareTo(arr[j-h]) < 0 ; j -= h)
                    arr[j] = arr[j-h];
                arr[j] = e;
            }
            h /= 3;
        }
    }

    public static void main(String[] args) {
        int[]a={49,38,65,97,76,13,27,49,78,34,12,64,1};
        System.out.println("排序之前：");
        for(int i=0;i<a.length;i++)
        {
            System.out.print(a[i]+" ");
        }
        //希尔排序
        int d=a.length;
        while(true)
        {
            d=d/2;
            for(int x=0;x<d;x++)
            {
                for(int i=x+d;i<a.length;i=i+d)
                {
                    int temp=a[i];
                    int j;
                    for(j=i-d;j>=0&&a[j]>temp;j=j-d)
                    {
                        a[j+d]=a[j];
                    }
                    a[j+d]=temp;
                }
            }
            if(d==1)
            {
                break;
            }
        }
        System.out.println();
        System.out.println("排序之后：");
        for(int i=0;i<a.length;i++)
        {
            System.out.print(a[i]+" ");
        }
    }
}











