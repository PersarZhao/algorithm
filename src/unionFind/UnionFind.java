package unionFind;

/**
 * Create by Persar on 2018/5/10
 * 更加完善的并查集，采用一个路径压缩（层数减少）
 * 当然在最理想的情况下是，每一个集合只有两层，但是由于递归花费本来就很大，所以效率与一般的路径压缩实际上
 * 差别不大，甚至略低
 */
public class UnionFind {

    private int[] parent;
    private int[] rank;
    private int count;

    public UnionFind(int n){
        parent = new int[n];
        rank = new int[n];
        count = n;
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            rank[i] = 1;
        }
    }

    //每次在查找某个元素的时候进行路径压缩
    public int find(int p){
        assert( p >= 0 && p < count );
        //第一种路径压缩，并不能保证最大高度为2，但是依旧很高效
        while (p != parent[p]){
            parent[p] = parent[parent[p]];
            p = parent[p];
        }
        return p;
        //第二种路径压缩：压缩成集合的所有元素都只指向根节点，也就是集合最大高度为2
//        if (p != parent[p]){
//            parent[p] = find( parent[p] );
//        }
//        return parent[p];
    }

    public boolean isConnected(int p,int q){
        return find(p) == find(q);
    }

    //合并集合
    public void union(int p, int q){
        int pRoot = find(p);
        int qRoot = find(q);
        if(pRoot == qRoot)
            return;
        if(rank[pRoot]<rank[qRoot]){
            parent[pRoot] = qRoot;
        }else if(rank[pRoot] > rank[qRoot]){
            parent[qRoot] = pRoot;
        }else{
            parent[pRoot] = qRoot;
            rank[qRoot] += 1;
        }
    }

}






















