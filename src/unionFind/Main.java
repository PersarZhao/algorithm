package unionFind;

import common.UnionFindTestHelper;

/**
 * Create by Persar on 2018/5/10
 * union的测试
 */
public class Main {
    // 测试UF1
    public static void main(String[] args) {

        // 使用10000的数据规模
        int n = 1000000;

        // 虽然isConnected只需要O(1)的时间, 但由于union操作需要O(n)的时间
        // 总体测试过程的算法复杂度是O(n^2)的
//        UnionFindTestHelper.testUF1(n);
//
//        // 对于UF2来说, 其时间性能是O(n*h)的, h为并查集表达的树的最大高度
//        // 这里严格来讲, h和logn没有关系, 不过大家可以简单这么理解
//        UnionFindTestHelper.testUF2(n);

//        // 根据集合的大小进行的union优化，在java语言中效果并不是很明显，在100000数据规模的时候时间花费更多
//        UnionFindTestHelper.testUF3(n);

        //根据生成的集合的高度来进行union操作，结果显而易见，可以轻松处理1000000级别的数据量
        UnionFindTestHelper.testUF4(n);
        UnionFindTestHelper.testUF5(n);
    }
}
