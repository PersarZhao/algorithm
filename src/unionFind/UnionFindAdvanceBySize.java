package unionFind;

/**
 * Create by Persar on 2018/5/10
 * 优化：对QuickUnion的union进行优化
 */
public class UnionFindAdvanceBySize {

    //指向父亲节点
    private int[] parent;
    //数据数量
    private int count;
    //每一个集合的大小
    private int[] size;

    public UnionFindAdvanceBySize(int n){
        parent = new int[n];
        count = n;
        size = new int[n];
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            size[i] = 1;
        }
    }

    //查找根节点O(h)
    public int find(int p){
        assert p >= 0 && p < count;
        while (p != parent[p])
            p = parent[p];
        return p;
    }

    //查看两个元素是否连接
    public boolean isConnected(int p , int q){
        return find(p) == find(q);
    }

    //合并元素q和元素p的集合
    //O(h)复杂度, h为树的高度
    public void union(int p ,int q){

        int pRoot = find(p);
        int qRoot = find(q);

        if(qRoot == pRoot)
            return;

        // 根据两个元素所在树的元素个数不同判断合并方向
        // 将元素个数少的集合合并到元素个数多的集合上
        if(size[pRoot] > size[qRoot]){
            parent[qRoot] = pRoot;
            size[pRoot] += size[qRoot];
        }else {
            parent[pRoot] = qRoot;
            size[qRoot] += size[pRoot];
        }
    }


}
