package unionFind;

/**
 * Create by Persar on 2018/5/10
 * 对union的优化
 */
public class UnionFindAdvanceByRank {

    private int[] parent;
    private int[] rank;
    private int count;

    public UnionFindAdvanceByRank(int n){
        parent = new int[n];
        rank = new int[n];
        count = n;
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            rank[i] = 1;//高度为1；
        }
    }

    // O(h)复杂度, h为树的高度
    public int find(int p){
        assert p >= 0 && p < count;
        while (p != parent[p])
            p = parent[p];
        return p ;
    }

    // 查看元素p和元素q是否所属一个集合
    // O(h)复杂度, h为树的高度
    public boolean isConnected( int p , int q ){
        return find(p) == find(q);
    }

    // 合并元素p和元素q所属的集合
    // O(h)复杂度, h为树的高度
    public void union(int p ,int q){

        int pRoot = find(p);
        int qRoot = find(q);

        if( pRoot == qRoot )
            return;
        // 根据两个元素所在树的高度不同判断合并方向
        // 将元素层数少的集合合并到元素个数多的集合上
        if( rank[pRoot] < rank[qRoot] ){
            parent[pRoot] = qRoot;
        }
        else if( rank[qRoot] < rank[pRoot]){
            parent[qRoot] = pRoot;
        }
        else{ // rank[pRoot] == rank[qRoot]
            parent[pRoot] = qRoot;
            rank[qRoot] += 1;   // 此时, 我维护rank的值
        }
    }
}















