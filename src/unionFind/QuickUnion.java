package unionFind;

/**
 * Create by Persar on 2018/5/10
 * 快速合并
 */
public class QuickUnion {

    //用数组构建一个指向父节点的树
    private int[] parent;
    //数据个数
    private int count;

    public QuickUnion(int n){
        parent = new int[n];
        count = n;
        //初始，让父亲节点指向自己
        for (int i = 0; i < n; i++) {
            parent[i] = i;
        }
    }

    //查找到当前元素的根节点
    public int find(int p){
        assert p >= 0 && p < count;
        while (p!=parent[p]){
            p = parent[p];
        }
        return p;
    }

    //判断两个元素是否连接
    public boolean isConnected(int p,int q){
        return find(p) == find(q);
    }

    //连接两个元素
    public void union(int p,int q){
        //将p根节点指向q节点的根节点
        int pRoot = find(p);
        int qRoot = find(q);
        if(qRoot == pRoot)
            return;
        parent[pRoot] = qRoot;
    }

}
















