package unionFind;

/**
 * Create by Persar on 2018/5/10
 * 并查集：解决的是连接问题，只是单纯的连接问题，判断两个元素是否有联系，而不确定两个元素之间的联系路径具体是什么
 * 也可以去实现数学中的集合
 *
 * 这个简单的实现时快速查找
 */
public class QuickFind {

    //第一版union-find本质上就是一个数组
    private int[] id;

    private int count;

    public QuickFind(int n ){
        id = new int[n];
        count = n;
        //初始化，每一个元素指向自己，相当于任何元素之间没有联系
        for (int i = 0; i < n; i++) {
            id[i] = i;
        }
    }

    //查找某一个id对应的集合编号---find
    public int find(int p){
        assert p >= 0 && p < count;
        return id[p];
    }

    // 查看元素p和元素q是否所属一个集合
    // O(1)复杂度
    public boolean isConnected(int p, int q){
        return find(p) == find(q);
    }

    //并元素---union
    //O(n)复杂度
    public void union(int p ,int q){
        int pId = find(p);
        int qId = find(q);
        if (pId == qId)
            return;
        for (int i = 0; i < count; i++) {
            if (id[i] == pId)
                id[i] = qId;
        }
    }

}




















