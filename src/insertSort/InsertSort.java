package insertSort;

import common.SortTestHelper;

/**
 * Create by Persar on 2018/5/7
 * 插入排序：O(n^2)
 * 对于近乎有序的数组，插入排序往往是效率非常高的，甚至高于O(NlogN)级别的排序算法
 */
public class InsertSort {

    private InsertSort(){}

    /**
     * 具体的排序算法
     * @param arr 待排序的数组
     * 核心在于寻找元素arr[i]合适的插入位置
     */
    public static void sort(Comparable[] arr){

        // 写法1
//            for( int j = i ; j > 0 ; j -- )
//                if( arr[j].compareTo( arr[j-1] ) < 0 )
                    //元素交换位置，由于元素交换存在三次赋值操作，所以性能稍微低一些
//                    swap( arr, j , j-1 );
//                else
//                    break;

        // 写法2
//            for( int j = i; j > 0 && arr[j].compareTo(arr[j-1]) < 0 ; j--)
//                swap(arr, j, j-1);
        //写法三
        for(int i = 1;i<arr.length;i++){
            Comparable data = arr[i];
            int j = i;
            for(;j>0 && data.compareTo(arr[j-1])<0;j--){
                arr[j] = arr[j-1];
            }
            arr[j] = data;
        }
    }

    /**
     * 插入排序
     * @param arr 待排序数组
     * @param l 数组起始位置
     * @param r 数组终止位置
     */
    public static void sort(Comparable[] arr, int l, int r) {
        for(int i = l;i<=r;i++){
            Comparable e = arr[i];
            int j = i;
            for(;j>l&&e.compareTo(arr[j-1])<0;j--){
                arr[j] = arr[j-1];
            }
            arr[j] = e;
        }
    }

    public static void main(String[] args) {
        int N = 20000;
        Integer[] arr = SortTestHelper.generateRandomArray(N, 0, 100000);
        sort(arr);
        SortTestHelper.printArray(arr);
        return;
    }


}




























