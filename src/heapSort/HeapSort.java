package heapSort;

import common.SortTestHelper;

/**
 * Create by Persar on 2018/5/8
 */
public class HeapSort {
    private HeapSort(){}

    /**
     * 堆排序
     * @param arr 待排序的数组
     */
    public static void sort(Comparable[] arr){
        int n = arr.length;
        heapSort(arr,n);
    }

    private static void heapSort(Comparable[] arr, int n) {
        //先把这个数组变成一个堆
        for (int i = (n - 1 - 1) / 2; i >= 0 ; i--) {
            shiftDown(arr,n,i);
        }

        //把最大的元素放在数组末尾，再次将前面(n-1)个元素的数组变一个堆
        for(int i = n-1;i > 0;i--){
            swap(arr,i,0);
            shiftDown(arr,i,0);
        }

    }

    private static void swap(Comparable[] arr, int i, int j) {
        Comparable temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private static void shiftDown(Comparable[] arr, int n, int i) {
        while(i*2+1<n){
            int j = i*2+1;
            if(j+1<n && arr[j].compareTo(arr[j+1])<0)
                j++;
            if(arr[i].compareTo(arr[j])>0)
                break;
            swap(arr,i,j);
            i = j;
        }
    }

    public static void main(String[] args){
        int N = 1000000;
        Integer[] arr = SortTestHelper.generateRandomArray(N, 0, 100000);
        SortTestHelper.testSort("heapSort.HeapSort", arr);
        return;
    }
}
