package heapSort;

import common.SortTestHelper;

import java.util.Arrays;

/**
 * Create by Persar on 2018/5/8
 * 排序测试
 */
public class Main {


    public static void main(String[] args) {
        // 比较 Merge Sort, 三种 Quick Sort 和三种 Heap Sort 的性能效率
        // 注意, 这几种排序算法都是 O(nlogn) 级别的排序算法

        int N = 1000000;

        // 测试1 一般性测试
        System.out.println("Test for random array, size = " + N + " , random range [0, " + N + "]");

        Integer[] arr1 = SortTestHelper.generateRandomArray(N, 0, N);
        Integer[] arr2 = Arrays.copyOf(arr1, arr1.length);
        Integer[] arr3 = Arrays.copyOf(arr1, arr1.length);
        Integer[] arr4 = Arrays.copyOf(arr1, arr1.length);
        Integer[] arr5 = Arrays.copyOf(arr1, arr1.length);
        Integer[] arr6 = Arrays.copyOf(arr1, arr1.length);
        Integer[] arr7 = Arrays.copyOf(arr1, arr1.length);

        SortTestHelper.testSort("mergeSort.MergeSortAdvance", arr1);
        SortTestHelper.testSort("quickSort.QuickSort", arr2);
        SortTestHelper.testSort("quickSort.QuickSort2Ways", arr3);
        SortTestHelper.testSort("quickSort.QuickSort3Ways", arr4);
        SortTestHelper.testSort("heapSort.HeapSort1", arr5);
        SortTestHelper.testSort("heapSort.HeapSort2", arr6);
        SortTestHelper.testSort("heapSort.HeapSort", arr7);

        System.out.println();


        // 测试2 测试近乎有序的数组
        int swapTimes = 100;
        assert swapTimes >= 0;

        System.out.println("Test for nearly ordered array, size = " + N + " , swap time = " + swapTimes);

        arr1 = SortTestHelper.generateNearlyOrderedArray(N, swapTimes);
        arr3 = Arrays.copyOf(arr1, arr1.length);
        arr4 = Arrays.copyOf(arr1, arr1.length);
        arr5 = Arrays.copyOf(arr1, arr1.length);
        arr6 = Arrays.copyOf(arr1, arr1.length);
        arr7 = Arrays.copyOf(arr1, arr1.length);

        SortTestHelper.testSort("mergeSort.MergeSortAdvance", arr1);
        SortTestHelper.testSort("quickSort.QuickSort2Ways", arr3);
        SortTestHelper.testSort("quickSort.QuickSort3Ways", arr4);
        SortTestHelper.testSort("heapSort.HeapSort1", arr5);
        SortTestHelper.testSort("heapSort.HeapSort2", arr6);
        SortTestHelper.testSort("heapSort.HeapSort", arr7);

        System.out.println();


        // 测试3 测试存在包含大量相同元素的数组
        System.out.println("Test for random array, size = " + N + " , random range [0,10]");

        arr1 = SortTestHelper.generateRandomArray(N, 0, 10);
        arr2 = Arrays.copyOf(arr1, arr1.length);
        arr3 = Arrays.copyOf(arr1, arr1.length);
        arr4 = Arrays.copyOf(arr1, arr1.length);
        arr5 = Arrays.copyOf(arr1, arr1.length);
        arr6 = Arrays.copyOf(arr1, arr1.length);
        arr7 = Arrays.copyOf(arr1, arr1.length);

        SortTestHelper.testSort("mergeSort.MergeSortAdvance", arr1);
        // 这种情况下, 普通的QuickSort退化为O(n^2)的算法, 不做测试
        //SortTestHelper.testSort("bobo.algo.QuickSort", arr2);
        SortTestHelper.testSort("quickSort.QuickSort2Ways", arr3);
        SortTestHelper.testSort("quickSort.QuickSort3Ways", arr4);
        SortTestHelper.testSort("heapSort.HeapSort1", arr5);
        SortTestHelper.testSort("heapSort.HeapSort2", arr6);
        SortTestHelper.testSort("heapSort.HeapSort", arr7);


        return;

    }
}
