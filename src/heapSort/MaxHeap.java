package heapSort;

import common.SortTestHelper;

/**
 * Create by Persar on 2018/5/8
 * 最大堆:
 *   每一个节点都大于其孩子节点
 */
public class MaxHeap {

    private Comparable[] data;
    private int size ;


    public MaxHeap(){
        this(10);
    }

    public MaxHeap(int capacity){
        data = new Comparable[capacity];
        size = 0;
    }

    public MaxHeap(Comparable[] arr){
        data = arr;
        //原地堆排序一下
        size = arr.length;
        int notLeaf = leftChild(size-1);
        while(notLeaf>=0){
            shiftDown(notLeaf);
            notLeaf--;
        }
    }

    public void add(Comparable x){
        data[size++] = x;
        //shiftUp
        shiftUp(size-1);
    }

    private void shiftUp(int k) {
        while(k>0&&data[k].compareTo(data[parent(k)])>0){
            swap(k,parent(k));
            k = parent(k);
        }
    }

    public Comparable remove(){
        Comparable result = data[0];
        data[0] = data[--size];
        shiftDown(0);
        return result;
    }

    private void shiftDown(int index) {
        while (leftChild(index)<size){
            int maxValueIndex = leftChild(index);
            if(maxValueIndex+1<size && data[maxValueIndex+1].compareTo(data[maxValueIndex])>0)
                maxValueIndex++;
            if(data[index].compareTo(data[maxValueIndex])>0)
                break;
            swap(index,maxValueIndex);
            index = maxValueIndex;
        }
    }

    private void swap(int i ,int j) {
        Comparable temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }


    //获取其父节点
    private int parent(int index){
        return (index - 1) / 2;
    }

    //获取左孩子节点
    private int leftChild(int index){
        return index * 2 + 1;
    }

    //获取右孩子节点
    private int rightChild(int index){
        return index * 2 + 2;
    }


    public static void main(String[] args){
        Integer[] a = new Integer[]{1,5,4,8,9,2,7};
        MaxHeap maxHeap = new MaxHeap(a);

//        for (int i = 0; i < a.length; i++) {
//            maxHeap.add(a[i]);
//        }
//        for (int i = 0; i < a.length; i++) {
//            System.out.println(maxHeap.data[i]);
//        }
        System.out.println();
        for (int i = 0; i < a.length; i++) {
            System.out.println(maxHeap.remove());
        }
    }




}


























