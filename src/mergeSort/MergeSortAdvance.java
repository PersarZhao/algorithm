package mergeSort;

import common.SortTestHelper;
import insertSort.InsertSort;

/**
 * Create by Persar on 2018/5/7
 * 归并排序的改进
 */
public class MergeSortAdvance {

    private MergeSortAdvance(){

    }

    /**
     * 对外开放的排序方法
     * @param arr 待排序的数组
     */
    public static void sort(Comparable[] arr){
        int n = arr.length;
        mergeSort(arr,0,n-1);
    }

    /**
     * 归，划分区域
     * @param arr 待排序数组
     * @param l 起始位置
     * @param r 终止位置
     */
    private static void mergeSort(Comparable[] arr, int l, int r) {
        //数组数量小于15的时候，采用插入排序进行优化
        if(r-l<=15){
            InsertSort.sort(arr,l,r);
            return;
        }
        int mid = (l+r)/2;
        mergeSort(arr,l,mid);
        mergeSort(arr,mid+1,r);
        //如果此时要归并的两个部分已经是有序的了，就不需要再进行归并的操作
        if(arr[mid].compareTo(arr[mid+1])>0)
            merge(arr,l,mid,r);
    }

    /**
     * 并，将归好的区域进行归并
     * @param arr 数组
     * @param l 起始位置
     * @param mid 中间位置
     * @param r 结束位置
     */
    private static void merge(Comparable[] arr, int l, int mid, int r) {
        Comparable[] aux = new Comparable[r-l+1];
        for(int i = l;i<=r;i++){
            aux[i-l] = arr[i];
        }
        int i = l;
        int j = mid + 1;
        for(int k = l;k<=r;k++){
            if(i>mid){
                arr[k] = aux[j-l];
                j++;
            }else if(j>r){
                arr[k] = aux[i-l];
                i++;
            }else if(aux[i-l].compareTo(aux[j-l])<0){
                arr[k] = aux[i-l];
                i++;
            }else{
                arr[k] = aux[j-l];
                j++;
            }
        }
    }

    public static void main(String[] args) {
        int N = 1000000;
        Integer[] arr = SortTestHelper.generateRandomArray(N, 0, 10000000);
        SortTestHelper.testSort("mergeSort.MergeSortAdvance", arr);
        return;
    }

}
