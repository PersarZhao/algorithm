package mergeSort;

import common.SortTestHelper;

/**
 * Create by Persar on 2018/5/7
 * 归并排序：O(NlogN)级别算法
 * 优点：快速，稳定
 * 缺点：需要使用额外的O(N)的空间
 */
public class MergeSort {

    private MergeSort(){}

    public static void sort(Comparable[] arr){
        int n = arr.length;
        mergeSort(arr,0,n-1);
    }

    /**
     * 归并排序的主要逻辑，在arr[l~r]
     * @param arr 要排序的数组
     * @param l 要排序的数组的起始位置
     * @param r 要排序的数组的终止位置
     */
    private static void mergeSort(Comparable[] arr, int l, int r) {
        //归并划分
        if(l>=r)
            return ;
        int mid = (r+l)/2;
        mergeSort(arr,l,mid);
        mergeSort(arr,mid+1,r);
        //归并
        merge(arr,l,mid,r);
    }

    /**
     * 归并操作
     * @param arr 要归并的数组
     * @param l 起始位置
     * @param mid 中间位置
     * @param r 结束位置
     */
    private static void merge(Comparable[] arr, int l, int mid, int r) {
        //复制一份
        Comparable[] aux = new Comparable[r-l+1];
        for(int i = l;i<=r;i++){
            aux[i-l] = arr[i];
        }
        // 初始化，i指向左半部分的起始索引位置l；j指向右半部分起始索引位置mid+1
        int i = l;
        int j = mid+1;
        for(int k = l;k<=r;k++){
            if(i>mid){ // 如果左半部分元素已经全部处理完毕
                arr[k] = aux[j-l];
                j++;
            }else if(j>r){ // 如果右半部分元素已经全部处理完毕
                arr[k] = aux[i-l];
                i++;
            }else if(aux[i-l].compareTo(aux[j-l])<0){ // 左半部分所指元素 < 右半部分所指元素
                arr[k] = aux[i-l];
                i++;
            }else{ // 左半部分所指元素 >= 右半部分所指元素
                arr[k] = aux[j-l];
                j++;
            }
        }
    }

    public static void main(String[] args) {
        // Merge Sort是我们学习的第一个O(nlogn)复杂度的算法
        // 可以在1秒之内轻松处理100万数量级的数据
        // 注意：不要轻易尝试使用SelectionSort, InsertionSort或者BubbleSort处理100万级的数据
        // 否则，你就见识了O(n^2)的算法和O(nlogn)算法的本质差异：）
        int N = 1000000;
        Integer[] arr = SortTestHelper.generateRandomArray(N, 0, 100000);
        SortTestHelper.testSort("mergeSort.MergeSort", arr);

        return;
    }
}
