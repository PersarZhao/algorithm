package binarySearch;


import java.util.LinkedList;

/**
 * Create by Persar on 2018/5/9
 * 二分搜索树O(logN):有局限性的，当插入的数据是近乎有序的情况，那么会退化成为O(n)级别
 */
public class BinarySearchTree<K extends Comparable<K>,V> {

    private class Node{
        public K key;
        public V value;
        public Node left;
        public Node right;

        public Node(K key,V value){
            this.key = key;
            this.value = value;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            return key.toString()+":"+value.toString();
        }
    }

    private int size;

    private Node root;

    public BinarySearchTree(){
        root = null;
        size = 0;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public int size(){
        return size;
    }

    /**
     * 添加一个元素
     * @param k
     * @param v
     */
    public void add(K k,V v){
      root = add(root,k,v);
    }

    private Node add(Node node, K k, V v) {
        if (node == null) {
            size ++;
            return new Node(k, v);
        }
        if(node.key.compareTo(k)==0)
            node.value = v;
        else if(node.key.compareTo(k)<0)
            node.left = add(node.left,k,v);
        else{
            node.right = add(node.right,k,v);
        }
        return node;
    }

    /**
     * 判断k是否存在
     * @param k
     * @return
     */
    public boolean contains(K k){
        if (root == null) {
            throw new IllegalArgumentException("二叉树为空!");
        }
        return contains(root,k);
    }

    private boolean contains(Node node, K k) {
        if(node == null)
            return false;
        else if(node.key.compareTo(k) == 0)
            return true;
        else if(node.key.compareTo(k)>0)
            return contains(node.right,k);
        else
            return contains(node.left,k);
    }

    /**
     * 查找索引k对应的值
     * @param k
     * @return
     */
    public V get(K k){
        return get(root,k);
    }

    private V get(Node node, K k) {
        if(node == null)
            return null;
        else if(node.key.compareTo(k) == 0)
            return node.value;
        else if (node.key.compareTo(k) > 0)
            return get(node.left,k);
        else
            return get(node.right,k);
    }

    /**
     * 获取二分搜索树中的最小值
     * @return
     */
    public V getMin(){
        if (root == null) {
            throw new IllegalArgumentException("二叉树为空");
        }
        return minNode(root).value;
    }
    //找到以node节点为根的树的最小节点
    private Node minNode(Node node) {
        if (node.left == null) {
            return node;
        }
        return minNode(node.left);
    }

    /**
     * 获取二分搜索树中的最大值
     * @return
     */
    public V getMax(){
        if (root == null) {
            throw new IllegalArgumentException("二叉树为空");
        }
        return maxNode(root).value;
    }

    private Node maxNode(Node node) {
        if(node.right == null)
            return node;
        return minNode(node.right);
    }


    /**
     * 修改值
     * @param k 索引
     * @param newV 新值
     */
    public void set(K k,V newV){
        if (root == null) {
            throw new IllegalArgumentException("二叉树为空");
        }
        set(root,k,newV);
    }

    private void set(Node node, K k, V newV) {
        if (node == null)
            throw new IllegalArgumentException("找不到为key为"+k+"的节点");
        if (node.key.compareTo(k) == 0)
            node.value = newV;
        else if(node.key.compareTo(k) > 0)
            set(node.left,k,newV);
        else
            set(node.right,k,newV);
    }

    /**
     * 删除最小元素所在的节点
     * @return
     */
    public void removeMin(){
        if (root == null) {
            throw new IllegalArgumentException("二叉树为空");
        }
        root = removeMin(root);
    }

    private Node removeMin(Node node) {
        if(node.left == null){
            Node curRight = node.right;
            node.right = null;
            size --;
            return curRight;
        }
        node.left = removeMin(node.left);
        return node;
    }

    /**
     * 删除最大元素
     * @return
     */
    public void removeMax(){
        if (root == null) {
            throw new IllegalArgumentException("二叉树为空");
        }
        root = removeMax(root);
    }

    private Node removeMax(Node node) {
        if (node.right == null){
            Node curLeft = node.left;
            node.left = null;
            size --;
            return curLeft;
        }
        node.right = removeMax(node.right);
        return node;
    }

    /**
     * 删除索引为k的元素
     * @param k
     * @return
     */
    public void remove(K k){
        if (root == null) {
            throw new IllegalArgumentException("二叉树为空");
        }
        root = remove(root,k);
    }

    private Node remove(Node node, K k) {
        if(node == null)
            throw new IllegalArgumentException("找不到该节点");
        if(node.key.compareTo(k) == 0) {
            //删除操作
            if (node.left == null) {
                Node curRight = node.right;
                node.right = null;
                size--;
                return curRight;
            }
            if (node.right == null) {
                Node curLeft = node.left;
                node.left = null;
                size--;
                return curLeft;
            }
            //获取到右子树的最小节点
            Node successor = minNode(node.right);
            successor.right = removeMin(node.right);
            successor.left = node.left;
            return successor;
        }
        else if (node.key.compareTo(k) > 0){
            node.left = remove(node.left,k);
        }else{
            node.right = remove(node.right,k);
        }
        return node;
    }

    /**
     * 返回<=k的最大节点
     * @param k
     * @return
     */
    public Node floor(K k){
        if(root == null)
            throw new IllegalArgumentException("二叉树为空!");
        return floor(root,k);
    }

    private Node floor(Node node, K k) {
        //等于当前节点
        if (k.compareTo(node.key) == 0)
            return node;
        //小于当前节点
        else if(k.compareTo(node.key) < 0){
            //当前节点没有左孩子了，并且此时k依然小于node.key
            if(node.left == null)
                return null;
            //大于当前节点的左孩子
            if (k.compareTo(node.left.key)>0)
                return node.left;
            return floor(node.left,k);
        }
        else{
            return floor(node.right,k);
        }
    }

    /**
     * 返回>=V的最小节点
     * @param k
     * @return
     */
    public Node ceil(K k){
        if(root == null)
            throw new IllegalArgumentException("二叉树为空!");
        return ceil(root,k);
    }

    private Node ceil(Node node, K k) {
        //等于当前节点
        if (k.compareTo(node.key) == 0)
            return node;
            //大于当前节点
        else if(k.compareTo(node.key) > 0){
            //并且当前节点没有右孩子了，相当于此时node节点最大，返回即可
            if(node.right == null)
                return node;
            //小于当前节点的右孩子
            if (k.compareTo(node.right.key)<0)
                return node;
            return ceil(node.right,k);
        }
        else{//小于当前节点
            return floor(node.left,k);
        }
    }

    //深度优先遍历，前中后序
    public void preOrder(){
        if(root == null)
            throw new IllegalArgumentException("二叉树为空!");
        preOrder(root);
    }

    private void preOrder(Node node) {
        if(node == null)
            return;
        System.out.println(node.key + ":" + node.value);
        preOrder(node.left);
        preOrder(node.right);
    }

    public void inOrder(){
        if(root == null)
            throw new IllegalArgumentException("二叉树为空!");
        inOrder(root);
    }

    private void inOrder(Node node) {
        if(node == null)
            return ;
        inOrder(node.left);
        System.out.println(node.key + ":" + node.value);
        inOrder(node.right);
    }

    public void postOrder(){
        if(root == null)
            throw new IllegalArgumentException("二叉树为空!");
        postOrder(root);
    }

    private void postOrder(Node node) {
        if(node == null)
            return ;
        postOrder(node.left);
        postOrder(node.right);
        System.out.println(node.key + ":" + node.value);
    }

    //广度优先遍历
    public void levelOrder(){
        if(root == null)
            throw new IllegalArgumentException("二叉树为空!");
        LinkedList<Node> list = new LinkedList<>();
        list.add(root);
        while (!list.isEmpty()){
            Node node = list.remove();
            System.out.println(node.key + ":" + node.value);
            if(node.left != null)
                list.add(node.left);
            if(node.right != null)
                list.add(node.right);
        }
    }
}





























