package binarySearch;

/**
 * Create by Persar on 2018/5/9
 * 二分查找:
 * 限制：必须是有序的数组
 */
public class BinarySearch {

    private BinarySearch(){}


    /**
     * 查找元素的索引
     * @param arr 元素所在数组,且此数组是有序的
     * @param e 待查找的元素
     * @return 如果存在，返回索引，如果不存在返回-1
     */
    public static int find(Comparable[] arr,Comparable e){
        int n = arr.length;
        //在[0,n-1]范围内进行查找
        int l = 0;
        int r = n - 1;
        while(l <= r){
            int mid = l + (r-l)/2;
            if(arr[mid].compareTo(e) == 0)
                return mid;
            else if (arr[mid].compareTo(e) > 0){
                l = mid + 1;
            }else{
                r = mid - 1;
            }

        }
        return -1;
    }

    /**
     * 二分查找法, 在有序数组arr中, 查找target
     * 如果找到target, 返回第一个target相应的索引index
     * 如果没有找到target, 返回比target小的最大值相应的索引, 如果这个最大值有多个, 返回最大索引
     * 如果这个target比整个数组的最小元素值还要小, 则不存在这个target的floor值, 返回-1
     * @param arr 排好序的数组
     * @param e 目标元素
     * @return
     */
    public static int floor(Comparable[] arr,Comparable e){
        if(arr[0].compareTo(e)>0){
            return -1;
        }
        int l = 0;
        int r = arr.length - 1;
        while(l < r){
            //避免计算的时候产生死循环，所以+1
            int mid = l + (r - l + 1) / 2;
            if(arr[mid].compareTo(e) >= 0){
                r = mid - 1;
            }else
                l = mid;
        }
        assert l == r;
        if(l + 1 < arr.length && arr[l+1].compareTo(e) == 0)
            return l+1;
        return l;
    }

    /**
     * 二分查找法, 在有序数组arr中, 查找target
     * 如果找到target, 返回最后一个target相应的索引index
     * 如果没有找到target, 返回比target大的最小值相应的索引, 如果这个最小值有多个, 返回最小的索引
     * 如果这个target比整个数组的最大元素值还要大, 则不存在这个target的ceil值, 返回整个数组元素个数n
     * @param arr 排好序的数组
     * @param e 目标元素
     * @return
     */
    public static int ceil(Comparable[] arr,Comparable e){
        int n = arr.length;
        if(arr[n-1].compareTo(e)<0)
            return n;
        int l = 0;
        int r = n - 1;
        while (r > l){
            //向下取整，避免死循环
            int mid = l + (r - l) / 2;
            if(arr[mid].compareTo(e)<= 0){
                l = mid + 1;
            }else {
                r = mid;
            }
        }
        assert l == r;
        if(r - 1 >= 0 && arr[r-1].compareTo(e) == 0)
            return r-1;
        return r;
    }

}




















