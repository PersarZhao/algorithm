package binarySearch;

/**
 * Create by Persar on 2018/5/9
 * 二分查找的递归写法
 */
public class BinarySearchRecursion {

    private BinarySearchRecursion(){}

    /**
     * 查找在数组arr中元素e的位置
     * @param arr 排好序的数组
     * @param e 待查找的元素
     * @return 查找到返回元素位置，没有查找到返回-1；
     */
    public static int find(Comparable[] arr,Comparable e){
        int n = arr.length;
        return find(arr,e,0,n-1);
    }

    /**
     * 核心递归查找，区间为[l,r]
     * @param arr 查找的数组
     * @param e 被查的元素
     * @param l 起始位置
     * @param r 最后位置
     * @return 找到返回索引，没找到返回-1
     */
    private static int find(Comparable[] arr, Comparable e, int l, int r) {
        if(l > r){
            return -1;
        }
        int mid = l + (r-l)/2;
        if(arr[mid].compareTo(e) == 0)
            return mid;
        else if(arr[mid].compareTo(e)>0)
            return find(arr,e,mid+1,r);
        else
            return find(arr,e,l,mid-1);
    }
}















