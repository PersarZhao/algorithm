package binarySearch;

import common.FileOperations;

import java.util.Vector;

/**
 * Create by Persar on 2018/5/9
 * 简单分词
 * 这个分词方式相对简陋, 没有考虑很多文本处理中的特殊问题
 * 在这里只做demo展示用
 */
public class Main {
    // 实验二分搜索树的局限性
    public static void main(String[] args) {

        // 我们使用文本量更小的共产主义宣言进行试验:)
        String filename = "./src/common/file/communist.txt";
        Vector<String> words = new Vector<String>();
        if (FileOperations.readFile(filename, words)) {
            System.out.println("There are totally " + words.size() + " words in " + filename);
            System.out.println();
            // 测试1: 我们按照文本原有顺序插入进二分搜索树
            long startTime = System.currentTimeMillis();
            BinarySearchTree<String, Integer> bst1 = new BinarySearchTree<String, Integer>();
            for (String word : words) {
                Integer res = bst1.get(word);
                if (res == null)
                    bst1.add(word, new Integer(1));
                else
                    bst1.add(word, res + 1);
            }
            System.out.println("二叉树大小:"+bst1.size());
            // 我们查看unite一词的词频
            if (bst1.contains("have"))
                System.out.println("'have' : " + bst1.get("have"));
            else
                System.out.println("No word 'have' in " + filename);

            long endTime = System.currentTimeMillis();

            System.out.println("BinarySearchTree , time: " + (endTime - startTime) + "ms.");
            System.out.println();
        }
    }
}
