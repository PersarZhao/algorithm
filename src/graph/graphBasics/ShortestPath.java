package graph.graphBasics;

import java.util.LinkedList;
import java.util.Stack;
import java.util.Vector;

/**
 * Create by Persar on 2018/5/10
 * 无权图的最短路径---广度优先遍历
 */
public class ShortestPath {

    private Graph graph;//图
    private boolean[] isVisit;//是否访问过了
    private int start;//起始节点
    private int[] from;//被访问的节点的前一个节点
    private int[] order;// 记录路径中节点的次序。ord[i]表示i节点在路径中的次序。

    public ShortestPath(Graph graph,int start){
        this.graph = graph;
        this.start = start;
        isVisit = new boolean[graph.getNodeNum()];
        from = new int[graph.getNodeNum()];
        order = new int[graph.getNodeNum()];
        for( int i = 0 ; i < graph.getNodeNum() ; i ++ ){
            isVisit[i] = false;
            from[i] = -1;
            order[i] = -1;
        }

        //用队列进行广度优先遍历
        LinkedList<Integer> queue = new LinkedList<>();
        queue.push(start);
        isVisit[start] = true;
        order[start] = 0;
        while(!queue.isEmpty()){
            int v = queue.pop();
            Iterable<Integer> integers = this.graph.adj(v);
            for(int i : integers){
                if (!isVisit[i]) {
                    queue.push(i);
                    isVisit[i] = true;
                    from[i] = v;
                    order[i] = order[v] + 1;
                }
            }
        }

    }

    // 查询从s点到w点是否有路径
    public boolean hasPath(int w){
        assert w >= 0 && w < graph.getNodeNum();
        return isVisit[w];
    }

    // 查询从s点到w点的路径, 存放在vec中
    public Vector<Integer> path(int w){

        assert hasPath(w) ;

        Stack<Integer> s = new Stack<Integer>();
        // 通过from数组逆向查找到从s到w的路径, 存放到栈中
        int p = w;
        while( p != -1 ){
            s.push(p);
            p = from[p];
        }

        // 从栈中依次取出元素, 获得顺序的从s到w的路径
        Vector<Integer> res = new Vector<Integer>();
        while( !s.empty() )
            res.add( s.pop() );
        return res;
    }

    // 打印出从s点到w点的路径
    public void showPath(int w){

        assert hasPath(w) ;

        Vector<Integer> vec = path(w);
        for( int i = 0 ; i < vec.size() ; i ++ ){
            System.out.print(vec.elementAt(i));
            if( i == vec.size() - 1 )
                System.out.println();
            else
                System.out.print(" -> ");
        }
    }
    // 查看从s点到w点的最短路径长度
    // 若从s到w不可达，返回-1
    public int length(int w){
        assert w >= 0 && w < graph.getNodeNum();
        return order[w];
    }
}
