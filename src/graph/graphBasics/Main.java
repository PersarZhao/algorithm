package graph.graphBasics;

import common.ReadGraph;

/**
 * Create by Persar on 2018/5/10
 * 测试图
 */
public class Main {
    public static void main(String[] args) {

        // 使用两种图的存储方式读取testG1.txt文件
        String filename = "./src/common/file/testG1.txt";
        SparseGraph g1 = new SparseGraph(13, false);
        ReadGraph readGraph1 = new ReadGraph(g1, filename);
        System.out.println("test G1 in Sparse Graph:");
        g1.show();

        System.out.println();

        DenseGraph g2 = new DenseGraph(13, false);
        ReadGraph readGraph2 = new ReadGraph(g2 , filename );
        System.out.println("test G1 in Dense Graph:");
        g2.show();

        System.out.println();

        // 使用两种图的存储方式读取testG2.txt文件
        filename = "./src/common/file/testG2.txt";
        SparseGraph g3 = new SparseGraph(6, false);
        ReadGraph readGraph3 = new ReadGraph(g3, filename);
        System.out.println("test G2 in Sparse Graph:");
        g3.show();

        System.out.println();

        DenseGraph g4 = new DenseGraph(6, false);
        ReadGraph readGraph4 = new ReadGraph(g4, filename);
        System.out.println("test G2 in Dense Graph:");
        g4.show();

         //TestG1.txt
        String filename1 = "./src/common/file/testG1.txt";
        SparseGraph g5 = new SparseGraph(13, false);
        ReadGraph readGraph5 = new ReadGraph(g5, filename1);
        Components component1 = new Components(g5);
        System.out.println("TestG1.txt, Component Count: " + component1.getcCount());

        int[] id = component1.getId();
        for (int i = 0;i<id.length;i++){
            System.out.print(id[i]+" ");
        }
        System.out.println();

        // TestG2.txt
        String filename2 = "./src/common/file/testG2.txt";
        SparseGraph g6 = new SparseGraph(6, false);
        ReadGraph readGraph6 = new ReadGraph(g6, filename2);
        Components component2 = new Components(g6);
        System.out.println("TestG2.txt, Component Count: " + component2.getcCount());

        id = component2.getId();
        for (int i = 0;i<id.length;i++){
            System.out.print(id[i]+" ");
        }

        filename = "./src/common/file/testG.txt";
        SparseGraph g = new SparseGraph(7, false);
        ReadGraph readGraph = new ReadGraph(g, filename);
        g.show();
        System.out.println();

        Path dfs = new Path(g,0);
        System.out.print("DFS : ");
        dfs.showPath(6);

        ShortestPath bfs = new ShortestPath(g,0);
        System.out.print("BFS : ");
        bfs.showPath(6);
    }
}
