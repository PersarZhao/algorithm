package graph.graphBasics;

import java.util.Vector;

/**
 * Create by Persar on 2018/5/10
 * 稠密图：每个节点之间基本上都有连接，叫做稠密图
 * 具体的可以看readme文件
 * 对于稠密图，一般采用邻接矩阵这种数据结构存储
 */
public class DenseGraph implements Graph{

    //节点数
    private int nodeNum;
    //边数
    private int sideNum;
    //是否是有向图
    private boolean directed;
    //邻接矩阵
    private boolean[][] graph;

    //初始化一个邻接矩阵
    public DenseGraph(int nodeNum , boolean directed){
        //节点数
        this.nodeNum = nodeNum;
        //一开始没有边
        this.sideNum = 0;
        this.directed = directed;
        //邻接矩阵false用来表示两个节点之间没有边
        graph = new boolean[nodeNum][nodeNum];
    }

    //返回节点的个数
    @Override
    public int getNodeNum(){
        return nodeNum;
    }
    //返回边的个数
    @Override
    public int getSideNum(){
        return sideNum;
    }

    //向图中两个节点间添加一个边
    @Override
    public void addSide(int v,int w){
        assert v >= 0 && v < nodeNum;
        assert w >= 0 && w <nodeNum;
        //查看两个节点之间是否有边
        if(hasSide(v,w))
            return;
        //添加边
        graph[v][w] = true;
        if(!directed)
            graph[w][v] = true;
        sideNum ++ ;
    }

    //返回某两个节点之间是否有边O(1)
    @Override
    public boolean hasSide(int v, int w) {
        assert v >= 0 && v < nodeNum ;
        assert w >= 0 && w < nodeNum ;
        return graph[v][w];
    }

    //将图打印出来
    @Override
    public void show() {
        for( int i = 0 ; i < nodeNum ; i ++ ){
            for( int j = 0 ; j < nodeNum ; j ++ )
                System.out.print(graph[i][j]+"\t");
            System.out.println();
        }
    }

    //返回图中某个节点的所有相邻节点
    @Override
    public Iterable<Integer> adj(int v){
        assert v >= 0 && v < nodeNum ;
        Vector<Integer> adjV = new Vector<Integer>();
        for (int i = 0; i < nodeNum; i++) {
            if(graph[v][i])
                adjV.add(i);
        }
        return adjV;
    }

}























