package graph.graphBasics;

import java.util.Stack;
import java.util.Vector;

/**
 * Create by Persar on 2018/5/10
 * 寻找路径(从节点A，到节点X的路径)
 * 深度优先遍历的路径，最长路径
 */
public class Path {

    //图
    private Graph graph;
    //起始点
    private int start;
    //记录某个节点是否访问过
    private boolean[] isVisit;
    //记录路径, from[i]表示查找的路径上i的上一个节点
    private int[] from;

    //在节点v处开始深度优先遍历
    public void dfs(int v){
        isVisit[v] = true;
        Iterable<Integer> integers = graph.adj(v);
        for(int i : integers){
            //如果当前i这个节点没有访问过
            if(!isVisit[i]) {
                //将i这个节点的前一个节点设置为v
                from[i] = v;
                dfs(i);
            }
        }
    }

    public Path(Graph graph,int start){
        this.graph = graph;
        this.start = start;
        isVisit = new boolean[graph.getNodeNum()];
        from = new int[graph.getNodeNum()];
        // 寻路算法
        dfs(start);
    }

    //判断从起始点start到w是否有路
    public boolean hasPath(int w){
        assert w >= 0 && w < graph.getNodeNum();
        return isVisit[w];
    }

    //存放一条从start到w的路径
    public Vector<Integer> path(int w){
        assert hasPath(w);

        Stack<Integer> s = new Stack<>();
        //将from数组中的内容放入栈中，然后再取出即可
        int p = w;
        while(p != start){
            s.push(p);
            p = from[p];
        }

        // 从栈中依次取出元素, 获得顺序的从s到w的路径
        Vector<Integer> res = new Vector<Integer>();
        while( !s.empty() )
            res.add( s.pop() );

        return res;
    }

    // 打印出从s点到w点的路径
    void showPath(int w){

        assert hasPath(w) ;

        Vector<Integer> vec = path(w);
        System.out.print(start+" -> ");
        for( int i = 0 ; i < vec.size() ; i ++ ){
            System.out.print(vec.elementAt(i));
            if( i == vec.size() - 1 )
                System.out.println();
            else
                System.out.print(" -> ");
        }
    }

}
