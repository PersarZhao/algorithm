package graph.graphBasics;

import java.util.Vector;

/**
 * Create by Persar on 2018/5/10
 * 稀疏图：每个节点拥有的边远远小于总的节点数
 * 具体的可以看readme文件
 * 对于稀疏图，一般采用邻接表这种数据结构存储
 */
public class SparseGraph implements Graph{

    //节点数
    private int nodeNum;
    //边数
    private int sideNum;
    //是否是有向图
    private boolean directed;
    //邻接表
    private Vector<Integer>[] graph;

    public SparseGraph(int nodeNum,boolean directed){
        this.nodeNum = nodeNum;
        this.sideNum = 0;
        this.directed = directed;
        graph = new Vector[nodeNum];
        for (int i = 0; i < nodeNum; i++) {
            graph[i] = new Vector<Integer>();
        }
    }
    @Override
    public int getNodeNum(){
        return this.nodeNum;
    }

    @Override
    public int getSideNum(){
        return this.sideNum;
    }

    //给 v,m添加一条边O(1)
    @Override
    public void addSide(int v,int w){
        assert v >= 0 && v < nodeNum;
        assert w >= 0 && w < nodeNum;

        if (hasSide(v,w))
            return;
        graph[v].add(w);
        if (!directed)
            graph[w].add(v);
        sideNum ++;
    }

    //判断v,w是否有边O(E),E表示这个节点的边数，或者说度数
    @Override
    public boolean hasSide(int v,int w){
        assert v >= 0 && v < nodeNum;
        assert w >= 0 && w < nodeNum;

        for(int i = 0;i<graph[v].size();i++){
            if(graph[v].elementAt(i) == w)
                return true;
        }
        return false;
    }

    @Override
    public void show() {
        for( int i = 0 ; i < nodeNum ; i ++ ){
            System.out.print("vertex " + i + ":\t");
            for( int j = 0 ; j < graph[i].size() ; j ++ )
                System.out.print(graph[i].elementAt(j) + "\t");
            System.out.println();
        }
    }

    //返回图中某个节点的所有相邻节点
    @Override
    public Iterable<Integer> adj(int v){
        assert v >= 0 && v < nodeNum;
        return graph[v];
    }

}













