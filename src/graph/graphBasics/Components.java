package graph.graphBasics;

/**
 * Create by Persar on 2018/5/10
 * 求一个图的连通分量
 */
public class Components {

    //求的是哪一张图
    private Graph G;
    //标记某个节点是否已经访问过了
    private boolean[] isVisit;
    //连通分量的个数
    private int cCount;
    //标记某个节点在哪一个连通分量里面
    private int[] id;

    public Components(Graph graph){
        G = graph;
        isVisit = new boolean[G.getNodeNum()];//默认全部是false
        cCount = 0;
        id = new int[G.getNodeNum()];//默认全部是0

        // 求图的联通分量
        for( int i = 0 ; i < G.getNodeNum() ; i ++ )
            if( !isVisit[i] ){
                cCount ++;
                dfs(i);

            }
    }

    //得到图的连通分量个数
    public int getcCount(){
        return cCount;
    }

    //
    public int[] getId(){
        return id;
    }

    //查询某两个点是否联通
    public boolean isConnected(int v,int w){
        assert v >= 0 && v < G.getNodeNum();
        assert w >= 0 && w < G.getNodeNum();
        return id[v] == id[w];
    }

    //图的深度优先遍历,从节点v开始
    public void dfs(int v){
        isVisit[v] = true;
        //将这个连通分量标记为cCount+1
        id[v] = cCount;
        //获取遍历当前节点的边的迭代器
        Iterable<Integer> iterable = G.adj(v);
        for (int i : iterable){
            if(!isVisit[i])
                dfs(i);
        }
    }
}















