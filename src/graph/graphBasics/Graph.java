package graph.graphBasics;

/**
 * Create by Persar on 2018/5/10
 */
public interface Graph {

    int getNodeNum();
    int getSideNum();
    void addSide( int v , int w );
    boolean hasSide( int v , int w );
    void show();
    Iterable<Integer> adj(int v);
}
