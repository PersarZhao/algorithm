package quickSort;

import common.SortTestHelper;
import insertSort.InsertSort;

import java.util.Arrays;

/**
 * Create by Persar on 2018/5/7
 * 三路快速排序，将整个数组分为了<v,=v,>v三个部分，每次递归<v,>v的部分
 * 其实移动的只有一个指针
 */
public class QuickSort3Ways {
    private QuickSort3Ways(){}

    public static void sort(Comparable[] arr){
        int n = arr.length;
        quickSort(arr,0,n-1);
    }

    private static void quickSort(Comparable[] arr, int l, int r) {
        if(r-l<=15){
            InsertSort.sort(arr,l,r);
            return;
        }
        //partition
        swap(arr,l,l+(int)(Math.random()*(r-l+1)));
        Comparable v = arr[l];
        //最后一个<v元素的索引位置
        int lt = l;
        //移动的索引
        int i = l+1;
        //第一个>v的元素的索引位置
        int rt = r+1;
        while (i<rt){
            if(arr[i].compareTo(v)<0){
                swap(arr,lt+1,i);
                lt++;
                i++;
            }else if(arr[i].compareTo(v)>0){
                swap(arr,rt-1,i);
                rt--;
            }else {
                i++;
            }
        }
        swap(arr,l,lt);


        quickSort(arr,l,lt-1);
        quickSort(arr,rt,r);
    }

    private static void swap(Comparable[] arr, int i, int j) {
        Comparable temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        int N = 1000000;
        System.out.println("Test for random array, size = " + N + " , random range [0,10]");

        Integer[] arr1 = SortTestHelper.generateRandomArray(N, 0, 10);

        SortTestHelper.testSort("quickSort.QuickSort3Ways", arr1);
    }

}
