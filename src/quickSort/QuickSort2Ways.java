package quickSort;

import common.SortTestHelper;
import insertSort.InsertSort;

/**
 * Create by Persar on 2018/5/7
 * 双路快速排序：使<=v,>=v的元素均匀的分布在v的两端,数组前后两端一起扫描，移动的有两个指针
 * 由此，便衍生出三路快速排序算法
 */
public class QuickSort2Ways {

    private QuickSort2Ways(){

    }

    public static void sort(Comparable[] arr){
        int n = arr.length;
        quickSort(arr,0,n-1);
    }

    private static void quickSort(Comparable[] arr, int l, int r) {
        if(r-l<=15){
            InsertSort.sort(arr,l,r);
            return;
        }
        int p = partition(arr,l,r);
        quickSort(arr,l,p-1);
        quickSort(arr,p+1,r);
    }

    private static int partition(Comparable[] arr, int l, int r) {

        //首先在[l-r]这个区间随机找一个值放在arr[l]处
        swap(arr,l,l+(int)(Math.random()*(r-l+1)));
        Comparable v = arr[l];
        int i = l+1;
        int j = r;
        while (true){
            while (i<=r && arr[i].compareTo(v)<0)
                i++;
            while (j>=l+1 && arr[j].compareTo(v)>0)
                j--;
            if(i > j)
                break;
            swap(arr,i,j);
            i++;
            j--;
        }
        swap(arr,l,j);
        return j;
    }

    private static void swap(Comparable[] arr, int i, int j) {
        Comparable temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    // 测试 QuickSort
    public static void main(String[] args) {

        // 双路快速排序算法也是一个O(nlogn)复杂度的算法
        // 可以在1秒之内轻松处理100万数量级的数据
        int N = 1000000;
        Integer[] arr = SortTestHelper.generateRandomArray(N, 0, 10);
        SortTestHelper.testSort("quickSort.QuickSort2Ways", arr);

        return;
    }
}
