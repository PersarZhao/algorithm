package quickSort;

import common.SortTestHelper;
import insertSort.InsertSort;

/**
 * Create by Persar on 2018/5/7
 * 第一版快速排序：O(NlogN)级别，虽然快速，但是有缺陷，对于近乎有序的数组来说，因为每一次的partition，都几乎没有将
 * 数据分成2块，而还是1块，最终导致此算法退化成为一个近乎O(n^2)的算法
 */
public class QuickSort {

    private QuickSort(){

    }

    public static void sort(Comparable[] arr){
        int n = arr.length;
        quickSort(arr,0,n-1);
    }

    private static void quickSort(Comparable[] arr, int l, int r) {
        if(r-l<=15){
            InsertSort.sort(arr,l,r);
            return;
        }
        int p = partition(arr,l,r);
        quickSort(arr,l,p-1);
        quickSort(arr,p+1,r);
    }

    private static int partition(Comparable[] arr, int l, int r) {
        Comparable v = arr[l];
        int i = l;
        for(int j = l+1;j<=r;j++){
            if(arr[j].compareTo(v)<0){
                i++;
                swap(arr,j,i);
            }
        }
        swap(arr,l,i);
        return i;
    }

    private static void swap(Comparable[] arr, int i, int j) {
        Comparable temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        // Quick Sort也是一个O(nlogn)复杂度的算法
        // 可以在1秒之内轻松处理100万数量级的数据
        int N = 1000000;
        Integer[] arr = SortTestHelper.generateRandomArray(N, 0, 1000000);
        SortTestHelper.testSort("quickSort.QuickSort", arr);

        return;
    }

}
