package quickSort;

import common.SortTestHelper;

import java.util.Arrays;

/**
 * Create by Persar on 2018/5/7
 * 测试比较归并排序，双路快速排序，三路快速排序
 * 经过测试，可以得出：
 * 在一般性数据中：3者的效率性能差不多，O(nlogn)级别
 * 在近乎有序的数据测试中，双路快排>归并>三路快排
 * 在有大量重复元素数据中，三路快排>双路快排>归并
 *
 * 一般性的快速排序有个弊端，是在近乎有序的数组时，近乎退化为O(n^2),所以采取了随机化的方式进行优化
 * 而随机化的方法，当存在有大量重复元素的时候，随机化的方式又近乎退化为O(n^2)，所以采取了双路快排，分散重复元素
 * 由双路快排中分散重复元素的思想--->三路快速排序
 */
public class Main {
    public static void main(String[] args) {
        int N = 1000000;

        // 测试1 一般性测试
        System.out.println("Test for random array, size = " + N + " , random range [0, " + N + "]");

        Integer[] arr1 = SortTestHelper.generateRandomArray(N, 0, N);
        Integer[] arr2 = Arrays.copyOf(arr1, arr1.length);
        Integer[] arr3 = Arrays.copyOf(arr1, arr1.length);

        SortTestHelper.testSort("mergeSort.MergeSortAdvance", arr1);
        SortTestHelper.testSort("quickSort.QuickSort2Ways", arr2);
        SortTestHelper.testSort("quickSort.QuickSort3Ways", arr3);

        System.out.println();


        // 测试2 测试近乎有序的数组
        // 双路快速排序算法也可以轻松处理近乎有序的数组
        int swapTimes = 100;
        assert swapTimes >= 0;

        System.out.println("Test for nearly ordered array, size = " + N + " , swap time = " + swapTimes);

        arr1 = SortTestHelper.generateNearlyOrderedArray(N, swapTimes);
        arr2 = Arrays.copyOf(arr1, arr1.length);
        arr3 = Arrays.copyOf(arr1, arr1.length);

        SortTestHelper.testSort("mergeSort.MergeSortAdvance", arr1);
        SortTestHelper.testSort("quickSort.QuickSort2Ways", arr2);
        SortTestHelper.testSort("quickSort.QuickSort3Ways", arr3);

        System.out.println();


        // 测试3 测试存在包含大量相同元素的数组
        // 使用双快速排序后, 我们的快速排序算法可以轻松的处理包含大量元素的数组
        System.out.println("Test for many seem array, size = " + N + " , random range [0,10]");

        arr1 = SortTestHelper.generateRandomArray(N, 0, 10);
        arr2 = Arrays.copyOf(arr1, arr1.length);
        arr3 = Arrays.copyOf(arr1, arr1.length);

        SortTestHelper.testSort("mergeSort.MergeSortAdvance", arr1);
        SortTestHelper.testSort("quickSort.QuickSort2Ways", arr2);
        SortTestHelper.testSort("quickSort.QuickSort3Ways", arr3);


        return;
    }
}
