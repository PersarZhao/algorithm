package quickSort;

import common.SortTestHelper;
import insertSort.InsertSort;

/**
 * Create by Persar on 2018/5/7
 * 快速排序的改进：每一次取标定值v的时候，随机在数组中选值，从而避免partition分区域严重不均的情况
 * 缺点：但是又有新的问题出现了，当这个数组里面有大量重复的数据的时候，随机选取的标定点会大概率的出现
 * 那些重复的数据，导致partition操作再次失效，算法近乎退化为O(n^2)
 * 双路快速排序可解决此问题
 */
public class QuickSortRandomV {

    private QuickSortRandomV(){

    }

    public static void sort(Comparable[] arr){
        int n = arr.length;
        quickSort(arr,0,n-1);
    }

    private static void quickSort(Comparable[] arr, int l, int r) {
        if(r-l<=15){
            InsertSort.sort(arr,l,r);
            return;
        }
        int p = partition(arr,l,r);
        quickSort(arr,l,p-1);
        quickSort(arr,p+1,r);
    }

    private static int partition(Comparable[] arr, int l, int r) {

        //首先在[l-r]这个区间随机找一个值放在arr[l]处
        swap(arr,l,l+(int)(Math.random()*(r-l+1)));
        Comparable v = arr[l];
        int i = l;
        for(int j = l+1;j<=r;j++){
            if(arr[j].compareTo(v)<0){
                i++;
                swap(arr,i,j);
            }
        }
        swap(arr,l,i);
        return i;
    }

    private static void swap(Comparable[] arr, int i, int j) {
        Comparable temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        // Quick Sort也是一个O(nlogn)复杂度的算法
        // 可以在1秒之内轻松处理100万数量级的数据
        int N = 1000000;
        Integer[] arr = SortTestHelper.generateRandomArray(N, 0, 100000);
        SortTestHelper.testSort("quickSort.QuickSortRandomV", arr);

        return;
    }
}
